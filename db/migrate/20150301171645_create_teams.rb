class CreateTeams < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.string :first_name
      t.string :designation
      t.string :picture

      t.timestamps null: false
    end
  end
end
