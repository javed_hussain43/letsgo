class AddColumnsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :first_name, :string, default: ""
    add_column :users, :last_name, :string, default: ""
    add_column :users, :company, :string, default: ""
    add_column :users, :designation, :string, default: ""
    add_column :users, :image, :string, default: ""
    add_column :users, :role, :string, default: ""
  end
end
