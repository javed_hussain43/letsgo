class Team < ActiveRecord::Base
	validates_uniqueness_of :first_name, scope: :designation
end
