class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  mount_uploader :image, AvatarUploader
  scope :standard_users, -> {where(role: 'standard')}

  def full_name
  	self.first_name+' '+self.last_name
  end   

  def admin?
  	self.role.eql? 'admin'
  end


   
end
