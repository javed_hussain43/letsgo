module ApplicationHelper

	def teams_email
		Team.all.map{|x| x.first_name.downcase+"@letsgomo.com"}
	end	

	def image_teams 
		Team.all.map{|x| x.first_name+"_"+x.designation}
  end
 
end
