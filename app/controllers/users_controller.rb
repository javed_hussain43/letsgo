require 'nokogiri'
require 'open-uri'
class UsersController < ApplicationController
  #load_and_authorize_resource
	
  def fetch_team
    doc = Nokogiri::HTML(open('http://letsgomo.com/meet-our-team/'))  	 
      doc.css('#content .container div.team-member.one-fourth').each do |record|
  	  image = record.children[0].attributes["src"]
  	  name = record.children[1].children.children.first.text
  	  designation = record.children[1].children.children[1].text
  	  Team.create(first_name: name, picture: image, designation: designation) 
    end
    redirect_to root_path
  end

  def send_mail

  end	

	def index
		@users = User.standard_users
	end 

	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		password = (0...50).map { ('a'..'z').to_a[rand(26)] }.join
	  @user.password = password
	  @user.password_confirmation = password

		if @user.save
			redirect_to users_admin_index_path_path, notice: 'User has been successfully created'
		else
			render 'new'
		end	
	end

	def edit
    @user = User.find(params[:id])
	end	

	def show
		@user = User.find(params[:id])
	end	

	def update
    @user = User.find(params[:id])
    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password].blank? and params[:user][:password_confirmation].blank?
    if @user.update_attributes(params[:user])
      flash[:notice] = "Successfully updated User."
      redirect_to root_path
    else
      render :action => 'edit'
    end
  end

  def compose_email
  	redirect_to root_path, notice: "Message has been successfully delivered."
  end	

	private
	def user_params
		params.require(:user).permit!
	end	
end
